# docker build -t csabasulyok/gradle:6.6.1-jdk8-alpine .
# docker login -u csabasulyok
# docker push csabasulyok/gradle:6.6.1-jdk8-alpine
# docker run -it --rm csabasulyok/gradle:6.6.1-jdk8-alpine sh

ARG JAVA_VERSION

FROM openjdk:${JAVA_VERSION}-jdk-alpine AS build-env
ARG GRADLE_VERSION
RUN apk update && apk add wget unzip
RUN wget https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip
RUN unzip -qq gradle-${GRADLE_VERSION}-bin.zip
RUN mv /gradle-${GRADLE_VERSION} /gradle
WORKDIR /gradle
RUN rm -rf LICENSE NOTICE README init.d bin/gradle.bat

FROM openjdk:${JAVA_VERSION}-jdk-alpine
MAINTAINER Csaba Sulyok <csaba.sulyok@gmail.com>
ENV GRADLE_HOME=/usr/lib/gradle
COPY --from=build-env /gradle/ ${GRADLE_HOME}/
ENV PATH=${PATH}:${GRADLE_HOME}/bin
CMD gradle
